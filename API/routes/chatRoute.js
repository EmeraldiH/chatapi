const express = require( 'express' )
const chatrouter = express.Router()
const ChatModel = require( '../Models/Chat' )

chatrouter.get('/', ( req, res ) => {
    ChatModel.find( )
    .then( data => res.json( data ) )
    .catch ( err=> res.json({message:err})) 
} )

chatrouter.get( '/:user', ( req, res ) => {
    ChatModel.find().or( [
        {
            $and: [ { "Recipient": { $eq: req.params.user } }, { "Status": "DELIVERED" }]
        },
        { "Sender": { $eq: req.params.user } }
    ] )
    .then( data => res.json( data ) )
    .catch ( err=> res.json({message:err}))
})

chatrouter.post('/', ( req, res ) => {
    const post = new ChatModel( {
        MsgId: req.body.MsgId,
        Message: req.body.Message,
        Sender: req.body.Sender,
        SendAt: req.body.SendAt,
        Recipient: req.body.Recipient,
        Status: req.body.Status
    })
    post.save()
        .then( data => res.json(data))
        .catch( err => res.json( { message: err }))
} )

chatrouter.post('/:msgId', ( req, res ) => {
    ChatModel.updateOne(
             { MsgId: req.params.msgId },
             { $set: { 
                "Status": "DELIVERED"
             }}
        ).then( data => res.json( data ) )
        .catch ( err=> res.json({message:err}))
})

module.exports = chatrouter;