const express = require( 'express' )
const router = express.Router()
const UserModel = require( '../Models/User' )


router.get('/', ( req, res ) => {
    UserModel.find( { "user_state": false })
    .then( data => res.json( data ) )
    .catch ( err=> res.json({message:err})) 
} )

router.get('/recipient/:id', ( req, res ) => {
    UserModel.find( { "_id":{$ne: req.params.id} })
    .then( data => res.json( data ) )
    .catch ( err=> res.json({message:err})) 
} )

router.get('/all', ( req, res ) => {
    UserModel.find( )
    .then( data => res.json( data ) )
    .catch ( err=> res.json({message:err})) 
} )

router.post( '/all', ( req, res ) => {
    UserModel.updateMany( { $set: { user_state: false } } )
    .then(data=>res.json(data))
})

router.get( '/state/:user', ( req, res ) => {
    UserModel.find( { 'username': { $eq: req.params.user } } )
        .then( data => res.json( data ) )
    .catch(err=>res.json({message:err}))
})

router.post( '/state/:id', ( req, res ) => {
     if ( req.body.user_state === true){
         UserModel.updateOne(
             { _id: req.params.id },
             { $set: { user_state: true } }
         )
             .then( data => res.json( data ) )
             .catch( err => res.json( { message: err } ))
    } else {
    UserModel.updateOne(
             { _id: req.params.id },
             { $set: { "user_state": false } }
        )
        .then( data => res.json( data ) )
        .catch ( err=> res.json({message:err}))
    }
})

router.post( '/', ( req, res ) => {
    const post = new UserModel({
        username: req.body.username,
        last_login: req.body.last_login
    })
    post.save()
        .then( data => res.json(data))
        .catch( err => res.json( { message: err }))
})

module.exports = router;