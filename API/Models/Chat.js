const mongoose = require( 'mongoose' )
require('mongoose-long')(mongoose);
const {Types: {Long}} = mongoose;

const ChatSchema = mongoose.Schema( {
    MsgId: {
        type: String,
        unique: true,
        required: true
    },
    Message: {
        type: String,
        required: true
    },
    Sender:{
        type: String,
        max:20,
        required:true
    },
    Recipient: {
        type: String,
        max:20,
        required:true
    },
    SendAt: {
        type: Long,
        required:true
    },
    Status: {
        type: String,
        default:"SENT"  
    }
}, { timestamps: true })

module.exports = mongoose.model('Chat', ChatSchema)