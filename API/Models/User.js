const mongoose = require( 'mongoose' )

const UserSchema = mongoose.Schema( {
    username: {
        type: String,
        unique: true,
        required: true
    },
    last_login: {
        type: String,
        default:Date.now
    },
    user_state:{
        type: Boolean,
        default:false
    }
} )

module.exports = mongoose.model('User', UserSchema)