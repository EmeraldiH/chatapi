const express = require( 'express' )
const app = express()
const fetch = require("node-fetch");
const amqp = require( 'amqplib/callback_api' )
const cors = require('cors')
corsOptions={
 cors: true,
 origins:["http://localhost:3000"],
}
const server = require('http').createServer(app);  
const io = require('socket.io')(server,corsOptions);
const mongoose = require( 'mongoose' )
const parser = require( 'body-parser' )

require( 'dotenv/config' )

app.use(cors())
app.use( parser.json() )  

const userRoute = require( './routes/userRoute' )
const chatRoute = require('./routes/chatRoute')

app.use( '/user', userRoute )
app.use('/chat',chatRoute)

mongoose.connect( process.env.DATABASE,
    { useNewUrlParser: true, useUnifiedTopology: true },
    () => {
    console.log("DB connected")
})


server.listen( 8080 )
amqp.connect( 'amqp://localhost', ( err, connection ) => {
    const users = []

    //<------------------------------------Connect User-------------------------------------->
    io.on( 'connection', ( socket ) => {
        console.log( 'new user connected' )
        socket.on( 'login', ( name ) => {
            console.log( 'user ' + name + ' connected' )
            let user = { 'socket': socket.id, 'username': name }
            let exist = false
            for ( let iter = 0; iter < users.length;iter++){
                if ( users[ iter ].username === name ) {
                    users[ iter ] = user
                    exist=true
                }
            }
            if ( !exist ) {
                users.push( user )
            }
            console.log( 'list of online users', users )
            
            fetch( 'http://localhost:9999/api/useronline/' + name )
                .then( response => {
                    connection.createChannel( ( channelErr, channel ) => {
                        let msgQueue = "readyToSend"
                        channel.assertQueue( msgQueue, { durable: false } )
                        channel.consume( msgQueue, ( messageQueue ) => {
                            let message = JSON.parse( messageQueue.content )
                            if ( message.Recipient !== name ) {
                                connection.createChannel( ( channelErr, resendChannel ) => {
                                    let msgQueue = 'readyToSend'
                                    resendChannel.assertQueue( msgQueue, { durable: false } )
                                    resendChannel.sendToQueue( msgQueue, Buffer.from( JSON.stringify( message ) ) )
                                })
                            } else {
                                console.log(name,message.Recipient)
                                if ( name === message.Recipient ) {
                                    io.to( socket.id ).emit( 'receiveQueue', ( { message } ) )
                                }}
                        }, {
                        noAck: true
                } )
            } )
        } )
    } )

        //<------------------------------------Kirim Message-------------------------------------->
        socket.on( 'messageSent', ( message ) => {
            for ( let iter = 0;  iter < users.length; iter++ ){
                if ( users[iter].username === message.Recipient ) {
                    io.to( users[ iter ].socket ).emit( 'receiveMessage', ( { message } ) )
                }
            }
            connection.createChannel( ( channelErr, sendChannel ) => {
                let msgQueue = 'pendingMessages'
                    sendChannel.assertQueue( msgQueue, { durable:false } )
                    sendChannel.sendToQueue( msgQueue, Buffer.from( JSON.stringify(message) ) )
            } )
        } )
        
        //<------------------------------------Disconnect User-------------------------------------->
        socket.on( 'disconnected', () => {
            for ( let iter = 0; iter < users.length; iter++ ){
                if ( users[iter].socket === socket.id ) {
                    users.splice( iter, 1 )
            }
            }
        })
    } )
})