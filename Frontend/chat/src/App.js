import './App.scss';
import Participant from './components/Participant';
function App () {
  return (
    <div className="App">
      <Participant />
    </div>
  );
}

export default App;
