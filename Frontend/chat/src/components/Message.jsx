import { Component } from "react";
export default class Message extends Component {
  render() {
    return (
      <div className={this.props.state}>
        <p>{this.props.sender}</p>
        <p>{this.props.message}</p>
        <p className="time">{this.props.time}</p>
      </div>
    );
  }
}
