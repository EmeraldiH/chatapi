import React, { Component } from "react";
import axios from "axios";
import io from "socket.io-client";
import { Button, Container, Row, Col, Form } from "react-bootstrap";
import Cookies from "universal-cookie";
import Messages from "./Message";
const cookie = new Cookies();
const API = "http://localhost:8080";
const BROKER = "http://localhost:9999";

export default class Participant extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: "",
      sender: "",
      senderId: "",
      users: [],
      users2: [],
      recipient: "",
      disable: false,
      userState: "disconnected",
      messsage: "",
      messagesData: [],
      senderValue: "",
    };
  }

  //<-----------------Terima Direct Message----------------------------------->
  receiveMessage = () => {
    this.socket.on("receiveMessage", ({ message }) => {
      if (this.state.messagesData.length === 0) {
        this.state.messagesData.push(message);
      }
      if (
        message.SendAt !==
        this.state.messagesData[this.state.messagesData.length - 1].SendAt
      ) {
        this.state.messagesData.push(message);
      }
      this.updateMessage(message.MsgId);
      this.messageReceived(message.MsgId)
      this.setState({ message: "" });
    });
  };

  //<-----------------------Konfirmasi Message Terkirim---------------------------------->
  messageReceived = (messageID) => {
    axios.get( BROKER + "/api/delivered/" + messageID )
  };

  //<-----------------------------Terima Message dari Queue------------------------------>
  receiveQueue = () => {
    this.socket.on("receiveQueue", ({ message }) => {
      if (this.state.messagesData.length === 0) {
        this.state.messagesData.push(message);
      }
      if (
        message.SendAt !==
        this.state.messagesData[this.state.messagesData.length - 1].SendAt
      ) {
        this.state.messagesData.push(message);
      }
      this.messageReceived(message.MsgId);
      this.updateMessage(message.MsgId );
      this.setState( { message: "" } )
    });
  };

  //<------------------Initialization-------------->
  componentDidMount = () => {
    this.socket = io(API);
    let userCookie = cookie.get("user");
    axios.get(API + "/user/all").then((res) => {
      let usersData = res.data;
      if (userCookie) {
        this.setState({
          senderId: userCookie.senderId,
          sender: userCookie.sender,
          users: usersData,
        });
        this.handleConnect();
      } else if (usersData.length > 0) {
        this.setState({
          users: usersData,
          senderId: usersData[0]._id,
          sender: usersData[0],
        });
      }
    });
  };

  //<-----------Iterasi option user & Recipient------------------->
  iterUsers = (userCol) => {
    let list = [];
    if (userCol.length === 0) {
      list.push(
        <>
          <option>No User Available</option>
        </>
      );
    } else {
      for (let iter = 0; iter < userCol.length; iter++) {
        let name = userCol[iter].username;
        list.push(
          <>
            <option value={iter}>{name}</option>
          </>
        );
      }
    }
    if (cookie.get("user")) {
      document.getElementsByName("sender").value = cookie.get("user").sender;
    }
    return <>{list}</>;
  };

  //<----------------Flag user as Online---------------->
  changeUserState = (param, id) => {
    let state = {
      user_state: param,
    };
    axios.post(API + "/user/state/" + id, state);
  };

  //<----------------------Request Message User ---------------->
  getMessages = () => {
    axios.get(API + "/chat/" + this.state.sender.username).then((res) => {
      for (let iter = 0; iter < res.data.length; iter++) {
        this.state.messagesData.push(res.data[iter]);
      }
      this.setState({ message: "" });
    });
  };

  //<--------------------------Connect User---------------------->
  handleConnect = (e) => {
    if (e) {
      e.preventDefault();
    }
    let user = {
      senderId: this.state.senderId,
      sender: this.state.sender,
    };
    cookie.set("user", user);
    let colRecipient = this.state.users.filter(
      (user) => user.username !== this.state.sender.username
    );
    this.setState({
      userState: "connected",
      disable: true,
      users2: colRecipient,
      recipient: colRecipient[0],
    });
    this.socket.emit("login", this.state.sender.username);
    this.changeUserState(true, this.state.sender._id);
    this.receiveQueue();
    this.receiveMessage();
    this.getMessages();
  };

  //<----------------------disconnect user------------------>
  handleDisconnect = (e) => {
    cookie.set("user", "");
    while (this.state.messagesData.length > 0) {
      this.state.messagesData.pop();
    }
    while (this.state.users2.length > 0) {
      this.state.users2.pop();
    }
    this.setState({
      userState: "disconnected",
      disable: false,
      recipient: "",
    });
    document.getElementsByName("sender").value = this.state.senderValue;
    this.socket.emit("disconnected");
    this.iterUsers(this.state.users2);
    this.changeUserState(false, this.state.sender._id);
  };

  //<-----------simpan message ke database------------------>
  saveMessage(msg) {
    axios.post(API + "/chat/", msg);
  }

  //<------------------------update status message------------->
  updateMessage(msgId) {
    axios.post(API + "/chat/" + msgId);
  }

  //<----------------------membuat tombol connect--------------->
  connect = () => {
    if (this.state.users.length < 1) {
      return <Button as="input" type="button" value="Connect" disabled />;
    } else if (this.state.userState === "disconnected") {
      return (
        <Button
          as="input"
          type="button"
          value="Connect"
          onClick={(e) => this.handleConnect(e)}
        />
      );
    } else {
      return (
        <Button
          as="input"
          type="button"
          value="Disconnect"
          onClick={(e) => this.handleDisconnect(e)}
        />
      );
    }
  };

  //<-------------menangkap perubahan state----------------->
  handleChange = (e) => {
    e.preventDefault();
    if (e.target.name === "sender") {
      this.setState({
        [e.target.name]: this.state.users[e.target.value],
        senderValue: [e.target.value],
      });
    } else if (e.target.name === "recipient") {
      this.setState({ [e.target.name]: this.state.users2[e.target.value] });
    } else {
      this.setState({ [e.target.name]: e.target.value });
    }
  };

  //<------------------menampilkan message------------------->
  iterMessage = () => {
    let list = [];
    for (let iter = 0; iter < this.state.messagesData.length; iter++) {
      if (
        this.state.messagesData[iter].Sender ===
          this.state.recipient.username ||
        this.state.messagesData[iter].Recipient ===
          this.state.recipient.username
      ) {
        let content = this.state.messagesData[iter].Message;
        let date = new Date(parseInt(this.state.messagesData[iter].SendAt));

        let day = date.getDate();
        let month = date.toLocaleString("default", { month: "long" });
        let hour = date.getHours();
        let minute = date.getMinutes();
        if (minute < 10) {
          minute = "0" + minute;
        }
        let time = hour + ":" + minute;
        let sendAt = month + " " + day + ", " + time;
        let state;
        if (
          this.state.messagesData[iter].Sender === this.state.sender.username
        ) {
          state = "send";
        } else {
          state = "receive";
        }

        list.push(
          <Messages
            sender={this.state.messagesData[iter].Sender}
            state={state}
            message={content}
            time={sendAt}
          />
        );
      }
    }
    return <>{list}</>;
  };

  //<---------------------Mengirim Message------------->
  handleSend = (e) => {
    e.preventDefault();
    if (this.state.message) {
      let Sender = this.state.sender.username;
      let Recipient = this.state.recipient.username;
      let SendAt = Date.now();
      let Message = this.state.message;
      let messageSent = {
        MsgId: Sender + SendAt,
        Sender,
        Recipient,
        SendAt: SendAt.toString(),
        Message,
        Status: "SENT",
      };
      this.socket.emit("messageSent", messageSent);
      this.state.messagesData.push(
        messageSent
      );
      this.setState({ message: "" });
      this.saveMessage( messageSent );
      console.log(messageSent);
    }
  };

  componentWillUnmount = (e) => {
    this.socket.emit("disconnected");
    this.handleDisconnect(e);
  };

  render() {
    return (
      <Container fluid="md" className="mainCont">
        <p>User</p>
        <Row>
          <Col sm={9}>
            <select
              name="sender"
              className="sender"
              onChange={(e) => this.handleChange(e)}
              disabled={this.state.disable}
            >
              {this.iterUsers(this.state.users)}
            </select>
          </Col>
          <Col>{this.connect()}</Col>
        </Row>
        <p>Recipient</p>
        <select
          name="recipient"
          className="recipient"
          onChange={(e) => this.handleChange(e)}
          disabled={!this.state.disable}
        >
          {this.iterUsers(this.state.users2)}
        </select>
        <div>
          <Form>
            <Form.Group>
              <Form.Label>Message</Form.Label>
              <Row>
                <Col>
                  <Form.Control
                    as="textarea"
                    name="message"
                    placeholder="Type a message..."
                    value={this.state.message}
                    onChange={(e) => this.handleChange(e)}
                    onKeyPress={(e) =>
                      e.key === "Enter" ? this.handleSend(e) : null
                    }
                  />
                </Col>
                <Col sm={3}>
                  <Button
                    as="input"
                    type="button"
                    value="Send"
                    onClick={(e) => this.handleSend(e)}
                    disabled={!this.state.disable}
                  />
                </Col>
              </Row>
            </Form.Group>
          </Form>
        </div>
        <Container className="messageCont">{this.iterMessage()}</Container>
      </Container>
    );
  }
}
